# ORKG Computer Science Named Entity Recognition


####  $`\textcolor{red}{\text{WARNING}}`$

> This ``README`` is not sufficient to re-create the dataset or re-train
the models because of missing information while migrating the codebase from the
old repository ([``orkg-nlp-experiments``](https://gitlab.com/TIBHannover/orkg/nlp/orkg-nlp-experiments)).
The [``main.py``](src/main.py) script is intended to prepare the service for 
integration into the [``orkgnlp``](https://gitlab.com/TIBHannover/orkg/nlp/orkg-nlp-pypi) package.


## Overview

### Aims
The ORKG CS-NER system is based on a standardized set of seven contribution-centric scholarly entities viz.,
research problem, solution, resource, language, tool, method, and dataset.
It can automatically extract all seven entity types from Computer Science publication titles.
Furthermore, it can extract research problem and method entity types from Computer Science publication abstracts.

### Approach
``...``

### Dataset
``...``

### Limitations 
``...``

### Useful Links
* ``...``
* ``...``

## How to Run

### Prerequisites

* Python version ``^3.71``



Running the main script with the following commands will create the needed
mapping files for the prediction and convert the pickled models to the TorchScript format. The output will be:
* [``orkgnlp-cs-ner-abstracts-alphabet.json``](data/processed/orkgnlp-cs-ner-abstracts-alphabet.json)
* [``orkgnlp-cs-ner-titles-alphabet.json``](data/processed/orkgnlp-cs-ner-titles-alphabet.json)
* [``orkgnlp-cs-ner-abstracts.pt``](models/orkgnlp-cs-ner-abstracts.pt)
* [``orkgnlp-cs-ner-titles.pt``](models/orkgnlp-cs-ner-titles.pt)

```commandline
git clone https://gitlab.com/TIBHannover/orkg/nlp/experiments/orkg-cs-ner
cd orkg-cs-ner
pip install -r requirements.txt
python -m src.main
```

## Contribution
This service is developed and maintained by:
* D'Souza, Jennifer <jennifer.dsouza@tib.eu>
* Arab Oghli, Omar <omar.araboghli@tib.eu>

## License
[MIT](./LICENSE)

## How to Cite

```commandline
@misc{dsouza2022computer,
      title={Computer Science Named Entity Recognition in the Open Research Knowledge Graph}, 
      author={Jennifer D'Souza and Sören Auer},
      year={2022},
      eprint={2203.14579},
      archivePrefix={arXiv},
      primaryClass={cs.CL}
}
```

## References

* [Computer Science Named Entity Recognition in the Open Research Knowledge Graph](https://arxiv.org/abs/2203.14579)

