from src.models import to_torchscript
from src.data import prepare_alphabet


def main():
    to_torchscript.main()
    prepare_alphabet.main()


if __name__ == '__main__':
    main()
