import os
import random
import torch

from src import MODELS_DIR, RAW_DATA_DIR
from src.models.ncrfpp import evaluation
from src.models.ncrfpp.model.seqlabel import SeqLabel
from src.models.ncrfpp.utils.data import Data

from src.util.io import read_pickle

MODEL_PATH = os.path.join(MODELS_DIR, 'orkgnlp-cs-ner-{0}.model')
DSET_PATH = os.path.join(RAW_DATA_DIR, 'orkgnlp-cs-ner-{0}.dset')

TEXTS = {
    'abstracts': [
        'Augmented Reality (AR) is a promising and growing field in marketing research and practice. Very little is known if, how, and why AR-apps can impact consumers’ perception and evaluation of brands. The following research presents and empirically tests a framework that theorizes how consumers perceive and evaluate the benefits and augmentation quality of AR apps, and how this evaluation drives subsequent changes in brand attitude. The study reveals consumer inspiration as a mediating construct between the benefits consumers derive from AR apps and changes in brand attitude. Besides providing novel insights into AR marketing theory, the study also suggests that marketers should consider evaluating mobile AR apps based on the inspiration potential (and not simply based on consumer attitudes, such as star-ratings in app stores).',
        'With the increasing popularity of graph structures, Graph embedding, Which aims to project nodes into low dimensional space while preserving the topological structure information of graphs and the information of nodes themselves, Has attracted an increased amount of attention in recent years. most of the embedding methods based on heterogeneous graphs use a meta-path guided random walk to capture the semantic and structural correlation between different types of nodes in the graph. despite the success of the meta-path-guided heterogeneous graph embedding method, The choice of meta-path is still an open and challenging problem. the design of the meta-path scheme largely depends on domain knowledge. in this paper, We propose a heterogeneous graph neural network with denoising (HGNND) to handle the issue. considering that there are different types of nodes in heterogeneous graphs, And their features are usually distributed in different spaces, The HGNND projects features of different types of nodes into a common vector space. then, The whole heterogeneous graph is input into the graph neural network to aggregate the neighbor node information and capture the structure information of the heterogeneous graph. finally, The noise nodes that may affect the performance of the whole model are filtered out by the denoising operation. extensive experiments on three real-world datasets demonstrate that our proposed model achieves state-of-the-art performance, It further proves that the model can still effectively aggregate semantic information without using meta-paths.',
        'Information systems that build on sensor networks often process data produced by measuring physical properties. These data can serve in the acquisition of knowledge for real-world situations that are of interest to information services and, ultimately, to people. Such systems face a common challenge, namely the considerable gap between the data produced by measurement and the abstract terminology used to describe real-world situations. We present and discuss the architecture of a software system that utilizes sensor data, digital signal processing, machine learning, and knowledge representation and reasoning to acquire, represent, and infer knowledge about real-world situations observable by a sensor network. We demonstrate the application of the system to vehicle detection and classification by measurement of road pavement vibration. Thus, real-world situations involve vehicles and information for their type, speed, and driving direction.',
        'Despite improved digital access to scholarly knowledge in recent decades, scholarly communication remains exclusively document-based. In this form, scholarly knowledge is hard to process automatically. We present the first steps towards a knowledge graph based infrastructure that acquires scholarly knowledge in machine actionable form thus enabling new possibilities for scholarly knowledge curation, publication and processing. The primary contribution is to present, evaluate and discuss multi-modal scholarly knowledge acquisition, combining crowdsourced and automated techniques. We present the results of the first user evaluation of the infrastructure with the participants of a recent international conference. Results suggest that users were intrigued by the novelty of the proposed infrastructure and by the possibilities for innovative scholarly knowledge processing it could enable.'
    ],
    'titles': [
        'Augmented reality marketing: How mobile AR-apps can improve brands through inspiration',
        'Heterogeneous Graph Neural Network with Hypernetworks for Knowledge Graph Embedding',
        'Situational Knowledge Representation for Traffic Observed by a Pavement Vibration Sensor Network',
        'Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge'
    ]
}


def convert_model(model, model_path, example_input):
    print("converting {}".format(model_path))

    scripted_model = torch.jit.script(model, example_inputs={model: [example_input]})

    scripted_model_name = '{}.pt'.format(os.path.splitext(os.path.split(model_path)[1])[0])
    scripted_model_path = os.path.join(MODELS_DIR, scripted_model_name)
    scripted_model.save(scripted_model_path)

    print('converted {}'.format(scripted_model_path))
    return scripted_model_path


def main():
    for type in TEXTS.keys():
        data = Data()
        data.load(read_pickle(DSET_PATH.format(type)))

        model = SeqLabel(data)
        data.generate_instance(random.choice(TEXTS[type]))
        model.load_state_dict(torch.load(MODEL_PATH.format(type), map_location=torch.device('cpu')))

        # Convert #
        batches = evaluation.get_model_input(data)
        batch_word, batch_features, batch_wordlen, batch_wordrecover, batch_char, batch_charlen, batch_charrecover, batch_label, mask = \
        batches[0]

        converted_model_path = convert_model(model, MODEL_PATH.format(type), example_input=(
            batch_word,
            batch_features,
            batch_wordlen,
            batch_char,
            batch_charrecover,
            mask,
            data.nbest
        ))

        # Verify #
        converted_model = torch.jit.load(os.path.join(MODELS_DIR, converted_model_path))
        test_conversion(data, model, converted_model, TEXTS[type])


def inference(batches, data, model):
    results = []
    model.eval()

    for batch in batches:
        batch_word, batch_features, batch_wordlen, batch_wordrecover, batch_char, batch_charlen, batch_charrecover, batch_label, mask = batch
        scores, nbest_tag_seq = model(batch_word, batch_features, batch_wordlen, batch_char, batch_charrecover, mask,
                                      data.nbest)
        tag_seq = nbest_tag_seq[:, :, 0]

        pred_label, gold_label = evaluation.recover_label(tag_seq, batch_label, mask, data.label_alphabet,
                                                          batch_wordrecover)
        results += pred_label

    return data.get_entities(results)


def test_conversion(data, model, converted_model, texts):
    for text in texts:
        data.generate_instance(text)
        batches = evaluation.get_model_input(data)

        entities = inference(batches, data, model)
        scripted_entities = inference(batches, data, converted_model)
        assert entities == scripted_entities


if __name__ == '__main__':
    main()
