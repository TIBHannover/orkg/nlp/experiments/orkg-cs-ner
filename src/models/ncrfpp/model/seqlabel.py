from __future__ import print_function
from __future__ import absolute_import

import torch.jit
import torch.nn as nn
from .wordsequence import WordSequence
from .crf import CRF


class SeqLabel(nn.Module):

    def __init__(self, data):
        super(SeqLabel, self).__init__()

        self.use_crf = data.use_crf
        self.gpu = data.HP_gpu
        self.average_batch = data.average_batch_loss

        # add two more label for downlayer lstm, use original label size for CRF
        label_size = data.label_alphabet_size
        data.label_alphabet_size += 2
        self.word_hidden = WordSequence(data)

        if self.use_crf:
            self.crf = CRF(label_size, self.gpu)

    def forward(self, word_inputs, feature_inputs, word_seq_lengths, char_inputs, char_seq_recover, mask, nbest):
        outs = self.word_hidden(word_inputs, feature_inputs, word_seq_lengths, char_inputs, char_seq_recover)
        scores, tag_seq = self.crf(outs, mask, nbest)
        return scores, tag_seq

