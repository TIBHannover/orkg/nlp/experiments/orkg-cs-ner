import os

from src import RAW_DATA_DIR, PROCESSED_DATA_DIR
from src.models.ncrfpp.utils.data import Data
from src.util.io import read_pickle, write_json

DSET_PATH = os.path.join(RAW_DATA_DIR, 'orkgnlp-cs-ner-{0}.dset')
ALPHABET_PATH = os.path.join(PROCESSED_DATA_DIR, 'orkgnlp-cs-ner-{}-alphabet.json')

TEXT_TYPES = ['abstracts', 'titles']


def main():
    alphabet = {}

    for type in TEXT_TYPES:
        data = Data()
        data.load(read_pickle(DSET_PATH.format(type)))

        alphabet['number_normalized'] = data.number_normalized
        alphabet['word'] = data.word_alphabet.instance2index
        alphabet['char'] = data.char_alphabet.instance2index
        alphabet['label'] = data.label_alphabet.instance2index

        write_json(alphabet, ALPHABET_PATH.format(type))


if __name__ == '__main__':
    main()
